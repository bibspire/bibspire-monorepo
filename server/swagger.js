const yaml = require('js-yaml');
const {promisify} = require('util');
const fs = require('fs');
const readFile = promisify(fs.readFile);

function swaggerUI(req, res) {
  res.send(`
<!DOCTYPE html><html><head><meta charset="UTF-8">
<link href="//unpkg.com/swagger-ui-dist@3.22.1/swagger-ui.css" rel="stylesheet" type="text/css"/>
</head><body><div id="swagger-ui"></div>
<script src="//unpkg.com/swagger-ui-dist@3.22.1/swagger-ui-bundle.js"></script>
<script>SwaggerUIBundle({
  url: "/v1/oas3-spec.json",dom_id: '#swagger-ui',
  displayRequestDuration: true,
  presets: [
    SwaggerUIBundle.presets.apis,
    SwaggerUIBundle.SwaggerUIStandalonePreset
  ]})
</script>
</body></html>`);
  return res.end();
}
async function swaggerSpec(req, res) {
  const spec = yaml.safeLoad(
    await readFile(__dirname + '/veduz-1.1.0-swagger.yaml', 'utf-8')
  );
  res.json(spec);
  res.end();
}
module.exports = {swaggerSpec, swaggerUI};
