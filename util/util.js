const fs = require('fs');
const crypto = require('crypto');
const {promisify} = require('util');
const CBOR = require('cbor');
const readline = require('readline');
const zlib = require('zlib');

const {cached, caches} = require('./cache');

function filterDuplicates(arr) {
  const seen = {};
  return arr.filter(o => {
    isSeen = seen[o];
    seen[o] = true;
    return !isSeen;
  });
}
function randomToken() {
  return crypto
    .randomBytes(12)
    .toString('base64')
    .replace(/[+]/g, '_')
    .replace(/[/]/g, '_');
}
function normaliseName(str) {
  return str
    .toLowerCase()
    .replace(/['"#]/g, '')
    .replace(/[-()\s/&+?.,;:]/g, ' ')
    .trim()
    .replace(/ +/g, '_')
    .slice(0, 250);
}
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
const readFile = promisify(fs.readFile);

async function forLineInFile(filename, f) {
  console.time('process lines in ' + filename);
  let fileStream = fs.createReadStream(filename);
  if (filename.endsWith('.gz')) {
    const gunzip = zlib.createGunzip();
    fileStream = fileStream.pipe(gunzip);
  }
  try {
    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity
    });
    for await (const line of rl) {
      await f(line);
    }
  } catch (e) {
    console.log(e);
  }
  fileStream.close();
  console.timeEnd('process lines in ' + filename);
}

module.exports = {
  forLineInFile,
  randomToken,
  normaliseName,
  sleep,
  readFile,
  cached,
  filterDuplicates,
  caches
};
