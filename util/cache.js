const Redis = require('ioredis');
const CBOR = require('cbor');

const caches = {
  manifest: new Redis({db: 1}),
  webtrekk_popularity: new Redis({db: 2}),
  webtrekk_sids: new Redis({db: 3}),
  webtrekk_wids: new Redis({db: 4}),
  pid2wid: new Redis({db: 5}),
  work: new Redis({db: 6}),
  usagedata_popularity: new Redis({db: 7}),
  usagedata_sids: new Redis({db: 8}),
  usagedata_wids: new Redis({db: 9}),
  adhl_popularity: new Redis({db: 7}),
  adhl_sids: new Redis({db: 8}),
  adhl_wids: new Redis({db: 9}),
  thumbnail: new Redis({db: 10})
};

let cacheInProgress = 0;
let maxCacheInProgress = 0;
let maxKeyLength = 0;
//setInterval(() => console.log('cacheInProgress', cacheInProgress), 60000);
function maxCache() {
  if (cacheInProgress > maxCacheInProgress) {
    maxCacheInProgress = cacheInProgress;
    if (maxCacheInProgress % 500 === 0) {
      console.log('maxCacheInProgress', cacheInProgress);
    }
  }
}
function cached(cache, nargs, fn) {
  const cacheName = cache;
  cache = caches[cache];
  return async function() {
    const args = Array.from(arguments).slice(0, nargs);
    const key = JSON.stringify(args);
    if (key.length > maxKeyLength) {
      maxKeyLength = key.length;
      console.log('cache max key length', key.length, cacheName, key);
    }
    ++cacheInProgress;
    maxCache();
    let result = await cache.getBuffer(key);
    if (result) {
      result = CBOR.decode(result);
    }
    --cacheInProgress;
    if (!result) {
      result = await fn.apply(null, args);
      ++cacheInProgress;
      maxCache();
      await cache.set(key, await CBOR.encodeAsync(result));
      --cacheInProgress;
    }
    return result;
  };
}

module.exports = {
  cached,
  caches
};
