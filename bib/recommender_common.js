const _ = require('lodash');

const {sleep} = require('../util/util');

const {getWork} = require('./metadata');
const webtrekk = require('./recommender_webtrekk');
const usagedata = require('./recommender_usagedata');
const adhl = require('./recommender_adhl');

async function recommend(
  {get_popularity, get_sids, get_wids},
  {wids, longtailedness, sampleSize}
) {
  if (longtailedness === undefined) {
    longtailedness = 0.5;
  }
  const sids = _.flatten(
    (await Promise.all(wids.map(get_sids))).map(arr =>
      arr.slice(0, sampleSize || 1000)
    )
  );
  const samples = sids.length;
  const related_wids = _.flatten(
    await Promise.all(sids.map(sid => get_wids(sid)))
  );
  let related = [];

  const freq = {};
  for (const wid of related_wids) {
    freq[wid] = (freq[wid] || 0) + 1;
  }
  for (const wid in freq) {
    related.push({wid: +wid, count: freq[wid]});
  }

  await Promise.all(
    related.map(async o => {
      o.popularity = await get_popularity(o.wid);
    })
  );

  await Promise.all(
    related.map(async o => {
      o.weight = o.count / Math.pow(o.popularity + 100, longtailedness);
    })
  );

  related.sort((a, b) => b.weight - a.weight);
  return {related, samples};
}

module.exports = {
  recommend
};

(async () => {
  /*
  await sleep(1000);
  //const wid = 596849;
  const wid = 1;
  console.time('webtrekk recommend');
  const a = await recommend(webtrekk, {wids: [wid]});
  console.timeEnd('webtrekk recommend');
  console.time('usagedata recommend');
  const b = await recommend(usagedata, {wids: [wid]});
  console.timeEnd('usagedata recommend');
  console.log("bib",a.related.length, b.related.length);

  /*
  for(o of a.related.slice(0,10)) {
    const w = await getWork(o.wid);
    console.log("bib",w.name, w.creator, o.weight, o.count, o.popularity, o.wid);
  }
  console.log("bib",);
  for(o of b.related.slice(0,10)) {
    const w = await getWork(o.wid);
    console.log("bib",w.name, w.creator, o.weight, o.count, o.popularity, o.wid);
  }
  */
})();
