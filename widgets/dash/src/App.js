import React from "react";
import axios from "axios";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Switch from "@material-ui/core/Switch";
import Typography from "@material-ui/core/Typography";
import moment from "moment";
import _ from "lodash";
import Chart from "chart.js";
import TextField from "@material-ui/core/TextField";

class ChartJS extends React.Component {
  constructor() {
    super();
    this.id = Math.random()
      .toString(36)
      .slice(2);
  }
  drawChart() {
    if (this.chart) {
      //console.log('XXX',this.chart.data);
      //this.chart.data.datasets.clear();
      //this.props.datasets.forEach(o => this.chart.data.datasets.push(o));
      Object.assign(this.chart.data, this.props.data);
      this.chart.update();
    }
  }
  componentDidMount() {
    this.elem = document.getElementById(this.id);
    this.chart = new Chart(this.elem, { ...this.props });
    this.drawChart();
  }
  render() {
    this.drawChart();
    return (
      <canvas
        height={this.props.height}
        width={this.props.width}
        id={this.id}
      />
    );
  }
}

// BibSpire Stat
function calculateHist(stat) {
  const result = {};
  const inc = elem => {
    for (const k in elem) {
      if (["count", "ms"].includes(k)) {
        continue;
      }
      if (!result[k]) {
        result[k] = {};
      }
      const v = elem[k];
      if (!result[k][v]) {
        result[k][v] = { count: 0, ms: 0 };
      }
      result[k][v].count += elem.count;
      result[k][v].ms += elem.ms;
    }
  };
  stat.forEach(inc);
  for (const k in result) {
    const t = [];
    for (const name in result[k]) {
      t.push({ name, ...result[k][name] });
    }
    t.sort((a, b) => b.count - a.count);
    result[k] = t;
  }
  return result;
}
function BibStat({ stat, state }) {
  if (!stat) {
    return <div />;
  }
  const hist = calculateHist(stat);

  let dates = stat.map(o => o.date);
  dates.sort();
  dates = _.sortedUniq(dates);
  const statByDate = _.groupBy(stat, o => o.date);
  let bibspireSum = 0;
  const referrerStat = dates.map(date => {
    const result = {
      bibspire: 0,
      bibspire_userstatus: 0,
      bibspire_collection: 0,
      "ting/object": 0
    };
    for (const o of statByDate[date]) {
      result[o.referrer] = result[o.referrer] + o.count;
    }
    result.bibspire_total =
      result.bibspire + result.bibspire_userstatus + result.bibspire_collection;
    bibspireSum += result.bibspire_total;
    result.bibspire_sum = bibspireSum;
    return result;
  });

  const beginAtZero = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    }
  };
  return (
    <div className="App">
      <Typography variant="h3">Statistik for BibSpire.</Typography>
      <br />
      <br />
      <ChartJS
        height={200}
        width={600}
        type="bar"
        data={{
          labels: dates,
          datasets: [
            {
              backgroundColor: "#88c",
              label:
                "Totalt antal klik på anbefalinger fra BibSpire siden periodens start",
              data: referrerStat.map(o => o.bibspire_sum)
            }
          ]
        }}
        options={beginAtZero}
      />

      <ChartJS
        height={200}
        width={600}
        type="bar"
        data={{
          labels: dates,
          datasets: [
            {
              backgroundColor: "#480",
              label: "Procent ekstra materialevisninger på grund af BibSpire",
              data: referrerStat.map(
                o => (100 * o.bibspire_total) / o["ting/object"]
              )
            }
          ]
        }}
        options={beginAtZero}
      />

      <ChartJS
        height={400}
        width={600}
        type="bar"
        data={{
          labels: dates,
          datasets: [
            {
              //backgroundColor: "#480",
              backgroundColor: "#066",
              label: "Materialevisninger på grund af BibSpire",
              data: referrerStat.map(o => o.bibspire_total)
            },
            {
              backgroundColor: "#ddd",
              label: "Øvrige materialevisninger",
              data: referrerStat.map(o => o["ting/object"])
            }
          ]
        }}
        options={beginAtZero}
      />

      <ChartJS
        height={400}
        width={600}
        type="bar"
        data={{
          labels: dates,
          datasets: [
            {
              backgroundColor: "#c88",
              label: "Klik på BibSpire-anbefalinger ved materialevisning",
              data: referrerStat.map(o => o.bibspire)
            },
            {
              backgroundColor: "#4c4",
              label: "Klik på BibSpire-anbefalinger ved brugerstatus",
              data: referrerStat.map(o => o.bibspire_userstatus)
            },
            {
              backgroundColor: "#44c",
              label: "Klik på BibSpire-anbefalinger ved værkoversigt",
              data: referrerStat.map(o => o.bibspire_collection)
            }
          ]
        }}
        options={beginAtZero}
      />
      <Typography>
        <b>Bemærk:</b> dette er et simpelt værktøj, og resultaterne kan
        indeholde usikkerhed, – så tjek tallen i jeres rigtige statistikløsning.
      </Typography>
      <hr />

      <Typography>
        Totalt: {_.sum(stat.map(o => o.count))} materialevisninger hvoraf{" "}
        {_.sum(
          stat.filter(o => o.referrer.startsWith("bibspire")).map(o => o.count)
        )}{" "}
        skyldtes BibSpire.
        <ul>
          {hist.library &&
            hist.library.map(({ count, name }) => (
              <li key={name}>
                {name}{" "}
                <small> {count} materialevisninger med anbefalinger</small>
              </li>
            ))}
        </ul>
      </Typography>
      <hr />

      <Typography>
        Usikkerheder ved metoden: statistikken er genereret ved at kigge på
        referrer-tag i webstatistik-loggen for recommender-api-requests. Nogle
        privacy-orienterede browsere sender ikke dette referrer-tag med.
        Robotter er genkendt ved at se om user-agent indeholder "bot", – dette
        fanger ikke alle robotter. Opsamlingen af statistikken er hurtigt
        skrevet – og der er en potentiel race-conditions, der kan gøre at
        enkelte data-punkter tabes.
      </Typography>
    </div>
  );
}
const subscribers = {
  aalborg: "Aalborg",
  ballerup: "Ballerup",
  brk: "Bornholm",
  brondby: "Brøndby",
  faxe: "Faxe",
  fredensborg: "Fredensborg",
  bbs: "Færøerne",
  guldbib: "Guldborgsund",
  hedensted: "Hedensted",
  herlev: "Herlev",
  hjbib: "Hjørring",
  holbaek: "Holbæk",
  jammerbugt: "Jammerbugt",
  lemvig: "Lemvig",
  mariagerfjord: "Mariager Fjord",
  middelfart: "Middelfart",
  naesbib: "Næstved",
  randers: "Randers",
  silkeborg: "Silkeborg",
  skanderborg: "Skanderborg",
  slagelsebib: "Slagelse",
  dcbib: "Sydslesvig",
  taarnby: "Taarnby",
  vejle: "Vejle",
  vhbib: "Vesthimmerland",
};
const bibspireSites = {
  default: "Alle biblioteker",
  ...subscribers,
  other: "Øvrige biblioteker"
};
function renderBibSpire(Settings, state, stat) {
  let { includebot, includedev, bibspireSite, fromDate, toDate } = state;

  stat = stat.filter(
    o =>
      o.path.startsWith("/v1/recommend") &&
      (o.referrer === "ting/object" || o.referrer.startsWith("bibspire")) &&
      o.status === 200
  );
  if (!includebot) {
    stat = stat.filter(
      o => o.useragent.indexOf("bot") === -1 && o.useragent !== "other"
    );
  }

  stat = stat.map(o => ({
    library: o.path.replace(/.*site=([^&]*).*/, (_0, s) => s),
    ...o
  }));
  const isDev = o =>
    o.library.endsWith("inlead.dk") ||
    o.library.endsWith("easysuite.dk") ||
    o.library.endsWith("ddbcms.dk") ||
    o.library.startsWith("staging.") ||
    o.library.startsWith("dev.") ||
    o.library.startsWith("nyfyn.");
  if (includedev) {
    stat = stat.filter(isDev);
  } else {
    stat = stat.filter(o => !isDev(o));
  }
  stat = stat.filter(
    o => o.library.endsWith(".dk") || o.library.endsWith(".fo")
  );
  if (Object.keys(subscribers).includes(bibspireSite)) {
    stat = stat.filter(o => o.library.indexOf(bibspireSite) !== -1);
  }
  if (bibspireSite === "other") {
    stat = stat.filter(
      o =>
        Object.keys(subscribers).filter(k => o.library.indexOf(k) !== -1)
          .length === 0
    );
  }
  if (fromDate) {
    stat = stat.filter(o => fromDate <= o.date);
  }
  if (toDate) {
    stat = stat.filter(o => toDate >= o.date);
  }
  return (
    <Container maxWidth="lg">
      <Settings />
      <hr style={{ margin: 32 }} />
      <BibStat stat={stat} state={state} />
    </Container>
  );
}

// Views
const views = {
  bibspire: "BibSpire",
  bibdata: "BibData",
  allsites: "Veduz / solsort sites"
};

class App extends React.Component {
  constructor() {
    super();
    let parsedHash = {};
    /*
    try {
      parsedHash = JSON.parse(
        decodeURIComponent(window.location.hash.slice(1))
      );
    } catch (e) {
      console.log(e);
    }
    */
    this.state = parsedHash;
    this.stat = [];
  }
  set(o) {
    this.setState(o);
    //setTimeout(() => (window.location.hash = JSON.stringify(this.state)), 0);
  }
  componentDidMount() {
    this.loadData();
  }
  async loadData() {
    this.set({ statState: "loading" });
    const result = await axios.get(`https://bibdata.dk/v1/stats/accesslog`);
    for (const date in result.data.dates) {
      const i = result.data.values;
      let stat = result.data.dates[date].map(o => {
        let [
          site,
          method,
          path,
          status,
          platform,
          useragent,
          referrer,
          count,
          ms
        ] = o;
        return {
          date,
          method: i.methods[method],
          path,
          site,
          status: status,
          platform: i.platforms[platform],
          referrer: i.referrers[referrer],
          useragent: i.useragents[useragent],
          count,
          ms
        };
      });

      this.stat = (this.stat || []).concat(stat);
    }

    this.set({ statState: "loaded" });
  }
  timeSelect() {
    return (
      <div>
        <Select
          value={this.state.view || "default"}
          onChange={e => this.set({ view: e.target.value })}
        >
          {Object.keys(views).map(view => (
            <MenuItem key={view} value={view}>
              {views[view]}
            </MenuItem>
          ))}
        </Select>
        <br />
        <TextField
          id="date"
          type="date"
          value={this.state.fromDate}
          onChange={o => this.set({ fromDate: o.target.value })}
        />
        &nbsp;_&nbsp;
        <TextField
          id="date"
          type="date"
          value={this.state.toDate}
          onChange={o => this.set({ toDate: o.target.value })}
        />
      </div>
    );
  }
  renderSettings() {
    return (
      <Grid container spacing={3}>
        <Grid item xs={4}>
          {this.timeSelect()}
        </Grid>
        <Grid item xs={4}>
          {this.state.view === "bibspire" && (
            <Select
              value={this.state.bibspireSite || "default"}
              onChange={e => this.set({ bibspireSite: e.target.value })}
            >
              {Object.keys(bibspireSites).map(s => (
                <MenuItem key={s} value={s}>
                  {bibspireSites[s]}
                </MenuItem>
              ))}
            </Select>
          )}
        </Grid>
        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Switch
                checked={!!this.state.includebot}
                onChange={() =>
                  this.set({ includebot: !this.state.includebot })
                }
              />
            }
            label="Inkludér besøg fra robotter"
          />
          <FormControlLabel
            control={
              <Switch
                checked={!!this.state.includedev}
                onChange={() =>
                  this.set({ includedev: !this.state.includedev })
                }
              />
            }
            label="Stagingsites"
          />
        </Grid>
      </Grid>
    );
  }
  render() {
    if (this.state.view === "bibspire") {
      return renderBibSpire(() => this.renderSettings(), this.state, this.stat);
    }
    return (
      <Container maxWidth="lg">
        <center>
          <Typography>Vælg løsning at vise statistik for:</Typography>
          {Object.keys(views).map(view => (
            <p key={view}>
              <Button onClick={() => this.set({ view })}>{views[view]}</Button>
            </p>
          ))}
        </center>
      </Container>
    );
  }
}

export default App;
